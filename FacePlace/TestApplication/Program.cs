﻿using FacePlace.DataLayer.Model;
using FacePlace.DataProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication
{
    class Program
    {
        static BusinessLogic business = Utility.GetBusinessLogic();

        static User Register(User user)
        {
            return business.Register(user);
        }
       
        static User Login(User user)
        {
            return business.Login(user);
        }

        public static Post AddUserPost(Post post)
        {
            return business.CreatePost(post);
        }

        static List<Post> GetRecentUserPosts(User user)
        {
            return business.GetRecentUserPosts(user);
        }

        static List<User> SearchUsers(string criteria)
        {
            return business.SearchUsers(criteria);
        }

        static List<Picture> PlaceGallery(string name)
        {
            return business.PlaceGallery(name);
        }

        static void main(string ar)
        {
            //foreach (Post p in business.NewsFeedRedis("Mare"))
            //{
            //    Console.WriteLine(p.Comment);
            //}
            foreach (Post p in business.NewsFeedNeo("Pera"))
            {
                Console.WriteLine(p.Comment);
            }

        }
    }
}
