﻿using FacePlace.DataLayer.Model;
using FacePlace.DataProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    class Program
    {
        static BusinessLogic dataSource = new BusinessLogic();

        static User Register(User user)
        {
            return dataSource.Register(user);           
        }

        static User Login(User user)
        {
            return dataSource.Login(user);
        }

        static void AddFriend(string username, string friend)
        {
            dataSource.AddFriend(username, friend);
        }

        static void CreatePost(Post post)
        {
            dataSource.CreatePost(post);
        }

        static List<Post> NewsFeedNeo(string username)
        {
            return dataSource.NewsFeedNeo(username);
        }

        static List<Post> NewsFeedRedis(string username)
        {
            return dataSource.NewsFeedRedis(username);
        }

        static void Main(string[] args)
        {
            User friend = new User()
            {
                Username = "drogos",
                Email = "drogos@gmail.com",
                Password = "golemanjiva==domovina",
                FirstName = "Marija",
                LastName = "Alexic"
            };

            User stefan = new User()
            {
                Username = "stefan81888",
                Email = "stefan81888@gmail.com",
                Password = "pridvorica==domovina",
                FirstName = "Stefan",
                LastName = "Stankovic"
            };

            //Register(friend);
            //Register(stefan);
            AddFriend("senta2708", "drogos");
            AddFriend("senta2708", "vucko");
            AddFriend("senta2708", "kix");
            AddFriend("senta2708", "tebron");




            Place bec = new Place()
            {
                Name = "Bec"
            };

            Place atina = new Place()
            {
                Name = "Athens"
            };

            Post post = new Post()
            {
                User = friend,
                Comment = "dunav",
                Place = bec
            };

            friend.Username = "tebron";

            Post post1 = new Post()
            {
                User = friend,
                Comment = "Uzoooooo",
                Place = atina
            };

            friend.Username = "vucko";

            CreatePost(post);
            CreatePost(post1);

            post1.Comment = "nemci";
            post1.Place = bec;

            
           
            CreatePost(post1);

            //foreach (var item in NewsFeedNeo("senta2708"))
            //{
            //    Console.WriteLine(item.Id + " " + item.Comment + " " + item.Place.Name);
            //}
        }
    }
}
