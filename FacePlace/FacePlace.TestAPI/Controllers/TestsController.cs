﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FacePlace.TestAPI.Controllers
{
    [RoutePrefix("api/tests")]
    public class TestsController : ApiController
    {
        BusinessLogic business = Utility.GetBusinessLogic();

        [HttpPost]
        [Route("register")]
        public User Register([FromBody]User user)
        {
            return business.Register(user);
        }

        [HttpPut]
        [Route("login")]
        public User Login([FromBody]User user)
        {
            return business.Login(user);
        }
    }
}
