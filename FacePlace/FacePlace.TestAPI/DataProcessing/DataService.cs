﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FacePlace.DataLayer.Utilities;
using FacePlace.DataLayer.DataLayerService;
using FacePlace.CashingSystem.CashingService;

using FacePlace.CashingSystem.Utilities;
using FacePlace.DataLayer.Model;

namespace DataProcessing
{
    public class DataService
    {
        private DataLayerService dataLayerService;
        private CashingService cashingService;

        public DataService()
        {
            dataLayerService = DataLayer.Utilities.ObjectFactory.GetDataLayerService();
            cashingService = CashingSystem.Utilities.ObjectFactory.GetCashingService();
        }

        public User GetUserFromCash(string username)
        {
            User user = cashingService.UserCash.GetUser(username);
            return user;
        }

        public void SaveUser(User user)
        {
            user = dataLayerService.UserRepository.Create(user);
            cashingService.UserCash.AddUserInformation(user);
        }

        public Post CreatePostId(Post post)
        {
            post.Id = cashingService.UserCash.CreatePostId();
            return post;
        }

        public void SavePostToCash(Post post)
        {
            string username = post.User.Username;
            cashingService.UserCash.AddUserPost(post);
            cashingService.PlaceCash.AddPlacePost(post);
            cashingService.PlaceCash.AddVisitor(post);
        }

        public void SavePostToDatabase(Post post)
        {
            dataLayerService.UserRepository.CreatePost(post);
        }

        public List<Post> GetRecentUserPost(User user)
        {
            string username = user.Username;
            return cashingService.UserCash.GetRecentPosts(username, 5);
        }

        public List<Post> GetRecentPlacePost(Place place)
        {
            string placeId = place.Id;
            return cashingService.PlaceCash.GetRecentPlacePosts(placeId, 5);
        }

        public List<User> WhoWasHereRecently(Place place)
        {
            string placeId = place.Id;
            return cashingService.PlaceCash.GetRecentVisitors(placeId, 10);
        }

        public List<User> SearchUsers(string criteria)
        {
            return dataLayerService.UserRepository.SearchUsers(criteria);
        }

        public List<Place> SearchPlaces(string criteria)
        {
            return dataLayerService.PlaceRepositiry.SearchPlaces(criteria);
        }

        public List<Picture> PlaceGallery(string placeName)
        {
            return dataLayerService.PictureRepository.GetGallery(placeName);
        }

        public Post AddPostPicturesToGallery(Post post)
        {
            cashingService.PlaceCash.AddPostPicturesToGallery(post);
            return post;
        }
    }
}