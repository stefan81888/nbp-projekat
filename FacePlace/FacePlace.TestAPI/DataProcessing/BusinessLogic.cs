﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataProcessing
{
    public class BusinessLogic
    {
        DataService dataService;

        public BusinessLogic()
        {
            dataService = Utility.GetDataService();             
        }

        public User Register(User registrationUser)
        {
            string username = registrationUser.Username;
            User user = dataService.GetUserFromCash(username);

            if (Utility.UserExists(user))
                return null;      

            dataService.SaveUser(user);
            return user;
        }

        public User Login(User loginUser)
        {
            string username = loginUser.Username;
            User user = dataService.GetUserFromCash(username);

            if (!Utility.UserExists(user))
                return null;

            return user;
        }

        public Post CreatePost(Post createdPost)
        {
            Post post = dataService.CreatePostId(createdPost);
            dataService.SavePostToCash(post);
            dataService.SavePostToDatabase(post);

            return post;
        } 

        public List<Post> GetRecentUserPosts(User user)
        {
            return dataService.GetRecentUserPost(user);
        }

        public List<Post> GetRecentPlacePosts(Place place)
        {
            return dataService.GetRecentPlacePost(place);
        }

        public List<User> WhoWasHereRecently(Place place)
        {
            return dataService.WhoWasHereRecently(place);
        }

        public List<User> SearchUsers(string criteria)
        {
            return dataService.SearchUsers(criteria);
        }

        public List<Place> SearchPlaces(string criteria)
        {
            return dataService.SearchPlaces(criteria);
        }

        public List<Picture> PlaceGallery(string placeName)
        {
            return dataService.PlaceGallery(placeName);
        }

        public Post AddPostPicturesToGallery(Post post)
        {
            dataService.AddPostPicturesToGallery(post);
            return post;
        }
    }
}