﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataProcessing
{
    public static class Utility
    {
        public static DataService GetDataService()
        {
            return new DataService();
        }

        public static BusinessLogic GetBusinessLogic()
        {
            return new BusinessLogic();
        }

        public static bool UserExists(User user)
        {
            return user.Username != string.Empty;
        }
    }
}